(function($){
	function imgcompare_activer() {
		$('.imgcompare:not(.started)').each(function() {
			let options = this.dataset.imgcompareOptions;
			if (options) {
				options = JSON.parse(options);
			}
			let view = new ImageCompare(this, options).mount();
			
			// On marque que c'est démarré
			$(this).addClass('started');
		});
	}

	$(function(){
		imgcompare_activer();
		onAjaxLoad(imgcompare_activer);
	});
})(jQuery);

<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function imgcompare_albums_decrire_dispositions($flux) {
	include_spip('inc/config');
	$largeur_config = lire_config('albums/img_largeur', null);
	$hauteur_config = lire_config('albums/img_hauteur', null);
	
	$flux['data']['compare'] = [
		'modele_album' => 'album_compare',
		'largeur' => $largeur_config,
		'hauteur' => $hauteur_config,
		'images' => true,
	];
	
	return $flux;
}

# Images Compare

![imgcompare](imgcompare-xx.svg)

Met à disposition un modèle afin de superposer deux images pour les comparer.

## Documentation

https://contrib.spip.net/Images-Compare

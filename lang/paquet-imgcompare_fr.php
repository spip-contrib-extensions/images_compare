<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	'imgcompare_description' => 'Ce plugin permet de comparer deux images dans un composant qui les superpose avec une barre déplaçable pour agrandir la vue de l’une ou l’autre des images. Un modèle permet de l’utiliser dans les contenus.',
	'imgcompare_nom' => 'Comparaison d’images',
	'imgcompare_slogan' => 'Comparer deux images superposée avec une barre de séparation pour passer de l’une à l’autre'
];
